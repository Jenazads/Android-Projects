# AndroidApps

Application Mobiles developed on Android OS

## Charger-Robot

Developed with [Edgar Huaranga](https://github.com/hued).

Is an application mobile to control a simple charger robot, using BlueTooth library, Arduino UNO, a CD drive, a part of car, 2 DC motors (wheels), a bluetooth device and actuators to pick up and put down targets.
